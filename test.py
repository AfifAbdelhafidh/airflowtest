from airflow import DAG
from airflow.operators import BashOperator, PythonOperator
from datetime import datetime, timedelta

# Following are defaults which can be overridden later on
default_args = {
    'owner': 'afif',
    'depends_on_past': False,
    'start_date': datetime(2021, 1, 10),
    'email': ['afif.renault@gmail.com'],
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 1,
    'retry_delay': timedelta(minutes=1),
}

dag = DAG('firstDag', default_args=default_args)

# t1, t2, t3 and t4 are examples of tasks created using operators

t1 = BashOperator(
    task_id='task_1',
    bash_command='echo "Task 1"',
    dag=dag)

t2 = BashOperator(
    task_id='task_2',
    bash_command='echo "Task 2"',
    dag=dag)

def run_Task_3:
    print('Task 3')

t3 = PythonOperator(
    task_id='task_3',
    python_callable=run_Task_3,
    dag=dag)

def run_Task_4:
    print('Run Task 4 from task 3')

t4 = PythonOperator(
    task_id='task_3',
    python_callable=run_Task_4,
    dag=dag)

t2.set_upstream(t1)
t3.set_upstream(t1)
t4.set_upstream(t3)